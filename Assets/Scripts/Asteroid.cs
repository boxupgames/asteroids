﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;

public class Asteroid : CryoBehaviour, IEnemy
{

    [Dependency]
    private GameEvents Events { get; set; }

    [Dependency("AsteroidsContainer")]
    private GameObject AsteroidsContainer { get; set; }

    public enum AsteroidSize { Big, Medium, Small };
    private AsteroidSize asteroidSize;

    private int asteroidScore;
    private float deadDistance;
    private float scale;
    private bool isOnField;
    private string lastWallName;

    private void Start()
    {
        Events.OnGameEnd += OnGameEnd_Action;

        asteroidScore = 0;
        deadDistance = 13f;
        isOnField = false;
        SetSize(AsteroidSize.Big);
    }

    private void FixedUpdate()
    {
        float dist = Vector3.Distance(Vector3.zero, transform.position);
        if (dist > deadDistance)
            DestroyAsteroid();
    }

    public void SetScore(int score)
    {
        asteroidScore = score;
    }

    public void SetSize(AsteroidSize size)
    {
        asteroidSize = size;

        switch (asteroidSize)
        {
            case AsteroidSize.Big:
                scale = .2f;
                break;
            case AsteroidSize.Medium:
                scale = .1f;
                break;
            case AsteroidSize.Small:
                scale = .05f;
                break;
        }

        transform.localScale = new Vector3(scale, scale, scale);
        ReduceMass();
    }

    public AsteroidSize GetSize()
    {
        return asteroidSize;
    }

    private void OnGameEnd_Action(object sender, EventArgs e)
    {
        DestroyAsteroid();
    }

    private void OnDestroy()
    {
        Events.OnGameEnd -= OnGameEnd_Action;
    }

    private void ReduceMass()
    {
        GetComponent<Rigidbody>().mass = GetComponent<Rigidbody>().mass / 2;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "WallReset" && !isOnField) isOnField = true;

        if (!isOnField) return;
        if (other.gameObject.name == "WallLeft")
        {
            transform.position = new Vector3(-(transform.position.x + 0.05f), transform.position.y, transform.position.z);
            isOnField = false;
            StartCoroutine(WallDelay());
        }
        if (other.gameObject.name == "WallRight")
        {
            transform.position = new Vector3(-(transform.position.x - 0.05f), transform.position.y, transform.position.z);
            isOnField = false;
            StartCoroutine(WallDelay());
        }
        if (other.gameObject.name == "WallFront")
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, -(transform.position.z - 0.05f));
            isOnField = false;
            StartCoroutine(WallDelay());
        }
        if (other.gameObject.name == "WallBack")
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, -(transform.position.z + 0.05f));
            isOnField = false;
            StartCoroutine(WallDelay());
        }
    }

    IEnumerator WallDelay()
    {
        yield return new WaitForSeconds(.1f);
        isOnField = true;
    }

    public void Hit()
    {

        if (asteroidSize != AsteroidSize.Small)
            CloneAsteroid();
        else
            DestroyAsteroid();

        switch (asteroidSize)
        {
            case AsteroidSize.Big:
                SetSize(AsteroidSize.Medium);
                break;
            case AsteroidSize.Medium:
                SetSize(AsteroidSize.Small);
                break;
            case AsteroidSize.Small:
                break;
        }

        Events.OnGetScore_FireEvent(asteroidScore);
    }

    private void CloneAsteroid()
    {
        GameObject asteroidClone = Instantiate(gameObject, transform.position, Quaternion.identity, AsteroidsContainer.transform);
        StartCoroutine(CloneAsteroidDelay(asteroidClone));
    }

    IEnumerator CloneAsteroidDelay(GameObject asteroidClone)
    {
        yield return new WaitForSeconds(0f);

        asteroidClone.GetComponent<Asteroid>().SetSize(asteroidSize);
        asteroidClone.GetComponent<Asteroid>().SetScore(asteroidScore);

        Rigidbody asteroidClone_rb = asteroidClone.GetComponent<Rigidbody>();
        asteroidClone_rb.AddForce(new Vector3(Randomer.RandomFloat(-1f, 1f), 0f, Randomer.RandomFloat(-1f, 1f)) * 100f);
    }

    private void DestroyAsteroid()
    {
        Destroy(gameObject);
    }

}

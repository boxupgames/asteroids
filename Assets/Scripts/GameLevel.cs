﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameLevel
{

    private int levelIndex;
    private int score;
    private int lives;
    private Level level;
    private List<Level> levels;

    public GameLevel(int levelNum)
    {
        if (levelNum == 1)
            InitFirstLevel();
    }

    public void Next()
    {
        levelIndex++;
        lives++;
        level = levels[levelIndex];
    }

    public void AddScore(int amount)
    {
        score += amount;
    }

    public void SubtractLive()
    {
        lives--;
    }

    public int GetLives()
    {
        return lives;
    }

    private void InitFirstLevel()
    {
        levelIndex = 0;
        score = 0;
        lives = 3;
        levels = FetchLevels();
        level = levels[levelIndex];
    }


    public LevelEventData CurrentLevelEventData()
    {
        return new LevelEventData
        {
            level = level,
            score = score,
            lives = lives,
            levelNum = level.num,
            isLevelNextExists = (levelIndex + 1 <= levels.Count - 1),
        };
    }

    private List<Level> FetchLevels()
    {
        List<Level> _levels = new List<Level>();
        _levels.Add(new Level
        {
            num = 1,
            asteroidSpeed = new Level.AsteroidSpeed(100f, 200f),
            asteroidsCount = 1,
            asteroidScore = 10,
        });
        _levels.Add(new Level
        {
            num = 2,
            asteroidSpeed = new Level.AsteroidSpeed(100f, 400f),
            asteroidsCount = 2,
            asteroidScore = 20,
        });
        _levels.Add(new Level
        {
            num = 3,
            asteroidSpeed = new Level.AsteroidSpeed(200f, 400f),
            asteroidsCount = 3,
            asteroidScore = 30,
        });
        _levels.Add(new Level
        {
            num = 4,
            asteroidSpeed = new Level.AsteroidSpeed(400f, 500f),
            asteroidsCount = 4,
            asteroidScore = 40,
        });
        _levels.Add(new Level
        {
            num = 5,
            asteroidSpeed = new Level.AsteroidSpeed(500f, 700f),
            asteroidsCount = 4,
            asteroidScore = 50,
        });
        return _levels;
    }

}

public class Level
{
    public int num;
    public struct AsteroidSpeed { public AsteroidSpeed(float min, float max) { Min = min; Max = max; } public float Min { get; } public float Max { get; } }
    public AsteroidSpeed asteroidSpeed;
    public int asteroidsCount;
    public int asteroidScore;
}

public class LevelEventData
{
    public int score;
    public int lives;
    public int levelNum;
    public Level level;
    public bool isLevelNextExists;
}

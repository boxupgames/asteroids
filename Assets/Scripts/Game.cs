﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;

public class Game : CryoBehaviour
{
    [Dependency]
    private GameEvents Events { get; set; }

    [Dependency("AsteroidsContainer")]
    private GameObject AsteroidsContainer { get; set; }

    private GameLevel gameLevel;
    private bool isEndDelay;

    void Start()
    {
//        Events.OnGameStart += OnGameStart_Action;
        Events.OnFire += OnFire_Action;
        Events.OnGetScore += OnGetScore_Action;
        Events.OnPlayerDie += OnPlayerDie_Action;
    }

    private void FixedUpdate()
    {
        ScanForAliveAsteroids();
    }

    private void ScanForAliveAsteroids()
    {
        if (gameLevel != null && GameObject.FindObjectOfType<Asteroid>() == null)
        {
            NextLevel();
        }
    }

    private void OnFire_Action(object sender, EventArgs e)
    {
        if (gameLevel == null && !isEndDelay)
        {
            Events.OnGameStart_FireEvent();
            gameLevel = new GameLevel(1);
            StartLevel();
        }
    }

    private void NextLevel()
    {
        if (gameLevel.CurrentLevelEventData().isLevelNextExists)
        {
            gameLevel.Next();
            StartLevel();
        } else
        {
            gameLevel = null;
            Events.OnGameEnd_FireEvent();
            StartCoroutine(EndDelay());
        }
    }

    private void OnPlayerDie_Action(object sender, EventArgs e)
    {
        if (gameLevel.GetLives() > 0)
        {
            gameLevel.SubtractLive();
            UpdateUI();
        } else
        {
            Events.OnGameEnd_FireEvent();
            Events.OnPlayerLost_FireEvent();
            gameLevel = null;
            StartCoroutine(EndDelay());
        }
    }

    IEnumerator EndDelay()
    {
        isEndDelay = true;
        yield return new WaitForSeconds(1f);
        isEndDelay = false;
    }

    /*    private void OnGameStart_Action(object sender, EventArgs e)
        {
            gameLevel = new GameLevel(1);
            Events.OnLevelStart_FireEvent(gameLevel.CurrentLevelEventData());
    //        StartCoroutine(StartLevel());
        }
    */
    private void StartLevel()
    {
        Events.OnLevelStart_FireEvent(gameLevel.CurrentLevelEventData());
        UpdateUI();
    }

    private void OnGetScore_Action(object sender, GameEvents.ScoreEventArgs e)
    {
        gameLevel.AddScore(e._score);
        UpdateUI();
    }

    private void UpdateUI()
    {
        LevelEventData data = gameLevel.CurrentLevelEventData();
        Events.OnScore_FireEvent(data.levelNum, data.score, data.lives);
    }


}

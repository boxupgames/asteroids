﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;

public class Player : CryoBehaviour
{
    [Dependency]
    private GameEvents Events { get; set; }

    [Dependency("GunStart")]
    private GameObject GunStart { get; set; }

    [Dependency("BulletContainer")]
    private GameObject BulletContainer { get; set; }

    public GameObject bulletPrefab;

    private Rigidbody rb;
    private float maxMagnitude = 5f;
    private float thrust = 70f;
    private float bulletSpeed = 10f;
    private bool isAccelerate;
    private bool isTurnLeft;
    private bool isTurnRight;
    private float turnVectorSpeed;
    private float slowdown;
    private string oppositeWallName;
    private bool isSafe;
    private bool isUnderControl;
    private bool isDead;
    private bool isOnField;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        turnVectorSpeed = 150f;
        slowdown = .97f;

        isAccelerate = false;
        isTurnLeft = false;
        isTurnRight = false;
        isSafe = true;
        isUnderControl = false;
        isDead = false;
        isOnField = true;

        Events.OnGameStart += OnGameStart_Action;
        Events.OnInputAcceleration += OnInputAcceleration_Action;
        Events.OnInputAccelerationRelease += OnInputAccelerationRelease_Action;
        Events.OnInputLeft += OnInputLeft_Action;
        Events.OnInputLeftRelease += OnInputLeftRelease_Action;
        Events.OnInputRight += OnInputRight_Action;
        Events.OnInputRightRelease += OnInputRightRelease_Action;
        Events.OnFire += OnFire_Action;
        Events.OnPlayerDie += OnPlayerDie_Action;
        Events.OnPlayerLost += OnPlayerLost_Action;

        StartSafe();
    }

    void FixedUpdate()
    {
        if (isUnderControl)
        {
            if (isAccelerate) Accelerate();
            if (!isAccelerate) Slow();

            if (isTurnLeft) TurnLeft();
            if (isTurnRight) TurnRight();
        }
    }

    private void Update()
    {
    }

    private void StartSafe()
    {
        if (isDead) return;
        StartCoroutine(SafeModeDelay());
    }

    IEnumerator SafeModeDelay()
    {
        isSafe = true;
        for (int i = 1; i <= 10; i++)
        {
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            yield return new WaitForSeconds(.3f);
            gameObject.GetComponent<MeshRenderer>().enabled = true;
            yield return new WaitForSeconds(.3f);
        }
        gameObject.GetComponent<MeshRenderer>().enabled = true;
        isSafe = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "WallReset" && !isOnField) isOnField = true;

        if (other == null) return;
        if (other.gameObject.name == "WallLeft")
        {
            transform.position = new Vector3(-(transform.position.x + .05f), transform.position.y, transform.position.z);
            isOnField = false;
            StartCoroutine(WallDelay());
        }
        if (other.gameObject.name == "WallRight")
        {
            transform.position = new Vector3(-(transform.position.x - .05f), transform.position.y, transform.position.z);
            isOnField = false;
            StartCoroutine(WallDelay());
        }
        if (other.gameObject.name == "WallFront")
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, -(transform.position.z - .05f));
            isOnField = false;
            StartCoroutine(WallDelay());
        }
        if (other.gameObject.name == "WallBack")
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, -(transform.position.z + .05f));
            isOnField = false;
            StartCoroutine(WallDelay());
        }

        if (other.gameObject.layer == 10)
        {
            Reset();
            StartSafe();
        }
    }

    IEnumerator WallDelay()
    {
        yield return new WaitForSeconds(.1f);
        isOnField = true;
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<IEnemy>() != null && !isSafe)
        {
            Events.OnPlayerDie_FireEvent();
        }
    }



    private void TurnLeft()
    {
        transform.Rotate(-Vector3.forward * turnVectorSpeed * Time.deltaTime);
    }

    private void TurnRight()
    {
        transform.Rotate(Vector3.forward * turnVectorSpeed * Time.deltaTime);
    }

    private void Accelerate()
    {
        if (rb.velocity.magnitude <= maxMagnitude)
            rb.AddForce(-transform.up * thrust * Time.deltaTime);
    }

    private void Slow()
    {
        rb.velocity *= slowdown;
    }

    private void Reset()
    {
        transform.position = new Vector3(0f, .5f, 0f);
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        isOnField = true;
    }

    private void Fire()
    {
        if (!isUnderControl) return;

        GameObject bullet = Instantiate(bulletPrefab, GunStart.transform.position, transform.rotation, BulletContainer.transform);
        Rigidbody bullet_rb = bullet.GetComponent<Rigidbody>();
        bullet_rb.AddForce(-GunStart.transform.up * (bulletSpeed + rb.velocity.magnitude));
    }

    private void HidePlayer()
    {
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<SphereCollider>().enabled = false;
    }

    private void ShowPlayer()
    {
        GetComponent<MeshRenderer>().enabled = true;
        GetComponent<SphereCollider>().enabled = true;
    }

    private void OnGameStart_Action(object sender, EventArgs e) { isUnderControl = true; isDead = false; ShowPlayer(); }
    private void OnPlayerLost_Action(object sender, EventArgs e) { isUnderControl = false; isDead = true; HidePlayer(); }
    private void OnInputAcceleration_Action(object sender, EventArgs e) { isAccelerate = true; }
    private void OnInputAccelerationRelease_Action(object sender, EventArgs e) { isAccelerate = false; }
    private void OnInputLeft_Action(object sender, EventArgs e) { isTurnLeft = true; }
    private void OnInputLeftRelease_Action(object sender, EventArgs e) { isTurnLeft = false; }
    private void OnInputRight_Action(object sender, EventArgs e) { isTurnRight = true; }
    private void OnInputRightRelease_Action(object sender, EventArgs e) { isTurnRight = false; }
    private void OnFire_Action(object sender, EventArgs e) { Fire(); }
    private void OnPlayerDie_Action(object sender, EventArgs e)
    {
        Reset();
        StartSafe();
    }

}

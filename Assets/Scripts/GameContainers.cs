﻿using UnityEngine;
using CryoDI;

public class GameContainers : UnityStarter
{

    protected override void SetupContainer(CryoContainer container)
    {
        container.RegisterSingleton<GameEvents>(LifeTime.Global);

        container.RegisterSceneObject<GameObject>("BulletContainer", "BulletContainer", LifeTime.Scene);
        container.RegisterSceneObject<GameObject>("AsteroidsContainer", "AsteroidsContainer", LifeTime.Scene);
        container.RegisterSceneObject<GameObject>("Player/Spaceship/GunStart", "GunStart", LifeTime.Scene);

        container.RegisterSceneObject<GameObject>("Space/AsteroidSpawnPointRight", "AsteroidSpawnPointRight", LifeTime.Scene);
        container.RegisterSceneObject<GameObject>("Space/AsteroidSpawnPointLeft", "AsteroidSpawnPointLeft", LifeTime.Scene);
        container.RegisterSceneObject<GameObject>("Space/AsteroidSpawnPointFront", "AsteroidSpawnPointFront", LifeTime.Scene);
        container.RegisterSceneObject<GameObject>("Space/AsteroidSpawnPointBack", "AsteroidSpawnPointBack", LifeTime.Scene);
    }

}

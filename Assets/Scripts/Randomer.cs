﻿using System;
using Random = System.Random;

public static class Randomer
{

    private static Random random;

    private static Random GetRandom()
    {
        if (random == null)
        {
            random = new Random(DateTime.Now.Millisecond); ;
        }
        return random;
    }

    public static bool RandomBool()
    {
        return GetRandom().Next(1, 3) % 2 == 0;
    }

    public static int RandomAmplitude()
    {
        return GetRandom().Next(2, 6);
    }

    public static float RandomDeelay()
    {
        return (float)GetRandom().Next(1, 4);
    }

    public static float RandomSpeed()
    {
        return (float)GetRandom().Next(1, 4);
    }

    public static int RandomNumber(int min, int max)
    {
        return GetRandom().Next(min, max);
    }

    public static float RandomFloat(float min, float max)
    {
        int _min = (int)(min * 100);
        int _max = (int)(max * 100);
        return GetRandom().Next(_min, _max) / 100f;
    }

    public static bool Between(float number, float min, float max)
    {
        return number >= min && number <= max;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;

public class InputControl : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }

    void Update()
    {
        EditorInput();
    }

    private void EditorInput()
    {

        if (Input.GetKeyDown(KeyCode.UpArrow))
            Events.OnInputAcceleration_FireEvent();
        if (Input.GetKeyUp(KeyCode.UpArrow))
            Events.OnInputAccelerationRelease_FireEvent();

        if (Input.GetKeyDown(KeyCode.LeftArrow))
            Events.OnInputLeft_FireEvent();
        if (Input.GetKeyUp(KeyCode.LeftArrow))
            Events.OnInputLeftRelease_FireEvent();

        if (Input.GetKeyDown(KeyCode.RightArrow))
            Events.OnInputRight_FireEvent();
        if (Input.GetKeyUp(KeyCode.RightArrow))
            Events.OnInputRightRelease_FireEvent();

        if (Input.GetKeyDown(KeyCode.Space))
            Events.OnFire_FireEvent();

        /* float horizontalValue = Input.GetAxis("Horizontal");
         float verticalValue = Input.GetAxis("Vertical");

         if (horizontalValue < 0)
         {
             print("Left");
         }

         if (horizontalValue > 0)
         {
             print("Right");
         }

         if (verticalValue < 0)
         {
             print("Down");
         }

         if (verticalValue > 0)
         {
             print("Up");
         }*/
    }

}

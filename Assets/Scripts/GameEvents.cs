﻿using System;
using UnityEngine;

public class GameEvents
{

    public event EventHandler OnGameLaunched;
    public event LevelEventHandler OnLevelStart;
    public event EventHandler OnLevelEnd;
    public event EventHandler OnGameStart;
    public event EventHandler OnGameEnd;
    public event EventHandler OnPlayerDie;
    public event EventHandler OnPlayerLost;

    public event EventHandler OnInputAcceleration;
    public event EventHandler OnInputAccelerationRelease;

    public event EventHandler OnInputLeft;
    public event EventHandler OnInputLeftRelease;

    public event EventHandler OnInputRight;
    public event EventHandler OnInputRightRelease;

    public event EventHandler OnFire;

    public event ScoreEventHandler OnGetScore;

    public event UpdateScoresUIEventHandler OnUpdateScoresUI;



    public delegate void ScoreEventHandler(object sender, ScoreEventArgs e);
    public void OnGetScore_FireEvent(int score)
    {
        ScoreEventArgs args = new ScoreEventArgs(score);
        OnGetScore?.Invoke(this, args);
    }
    public class ScoreEventArgs : EventArgs
    {
        public int _score { get; set; }
        public ScoreEventArgs(int score)
        {
            _score = score;
        }
    }

    public delegate void LevelEventHandler(object sender, LevelEventArgs e);
    public void OnLevelStart_FireEvent(LevelEventData levelData)
    {
        LevelEventArgs args = new LevelEventArgs(levelData);
        OnLevelStart?.Invoke(this, args);
    }
    public class LevelEventArgs : EventArgs
    {
        public LevelEventData _levelData { get; set; }
        public LevelEventArgs(LevelEventData levelData)
        {
            _levelData = levelData;
        }
    }

    public delegate void UpdateScoresUIEventHandler(object sender, UpdateUIEventArgs e);
    public void OnScore_FireEvent(int level, int score, int lives)
    {
        UpdateUIEventArgs args = new UpdateUIEventArgs(level, score, lives);
        OnUpdateScoresUI?.Invoke(this, args);
    }
    public class UpdateUIEventArgs : EventArgs
    {
        public int _level { get; set; }
        public int _score { get; set; }
        public int _lives { get; set; }
        public UpdateUIEventArgs(int level, int score, int lives)
        {
            _level = level;
            _score = score;
            _lives = lives;
        }
    }

    public void OnGameLaunched_FireEvent() { OnGameLaunched?.Invoke(this, EventArgs.Empty); }
    public void OnInputAcceleration_FireEvent() { OnInputAcceleration?.Invoke(this, EventArgs.Empty); }
    public void OnInputAccelerationRelease_FireEvent() { OnInputAccelerationRelease?.Invoke(this, EventArgs.Empty); }
    public void OnInputLeft_FireEvent() { OnInputLeft?.Invoke(this, EventArgs.Empty); }
    public void OnInputLeftRelease_FireEvent() { OnInputLeftRelease?.Invoke(this, EventArgs.Empty); }
    public void OnInputRight_FireEvent() { OnInputRight?.Invoke(this, EventArgs.Empty); }
    public void OnInputRightRelease_FireEvent() { OnInputRightRelease?.Invoke(this, EventArgs.Empty); }
    public void OnFire_FireEvent() { OnFire?.Invoke(this, EventArgs.Empty); }
    public void OnGameStart_FireEvent() { OnGameStart?.Invoke(this, EventArgs.Empty); }
    public void OnGameEnd_FireEvent() { OnGameEnd?.Invoke(this, EventArgs.Empty); }
    public void OnLevelEnd_FireEvent() { OnLevelEnd?.Invoke(this, EventArgs.Empty); }
    public void OnPlayerDie_FireEvent() { OnPlayerDie?.Invoke(this, EventArgs.Empty); }
    public void OnPlayerLost_FireEvent() { OnPlayerLost?.Invoke(this, EventArgs.Empty); }

}

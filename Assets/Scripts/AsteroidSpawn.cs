﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;

public class AsteroidSpawn : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }

    [Dependency("AsteroidsContainer")]
    private GameObject AsteroidsContainer { get; set; }

    public enum SpawnerPosition { Left, Right, Front, Back };
    public SpawnerPosition spawnerPosition;

    public GameObject asteroidPrefab;

    private void Start()
    {
        Events.OnLevelStart += OnLevelStart_Action;
    }

    private void OnLevelStart_Action(object sender, GameEvents.LevelEventArgs e)
    {
        int count = e._levelData.level.asteroidsCount;

        if (spawnerPosition == SpawnerPosition.Front && count >= 1)
            FireAsteroid(new Vector3(Randomer.RandomFloat(0f, 1f), 0f, -1f) * Randomer.RandomFloat(e._levelData.level.asteroidSpeed.Min, e._levelData.level.asteroidSpeed.Max), e._levelData.level.asteroidScore);
        if (spawnerPosition == SpawnerPosition.Back && count >= 2)
            FireAsteroid(new Vector3(Randomer.RandomFloat(0f, 1f), 0f, 1f) * Randomer.RandomFloat(e._levelData.level.asteroidSpeed.Min, e._levelData.level.asteroidSpeed.Max), e._levelData.level.asteroidScore);
        if (spawnerPosition == SpawnerPosition.Left && count >= 3)
            FireAsteroid(new Vector3(1f, 0f, Randomer.RandomFloat(0f, 1f)) * Randomer.RandomFloat(e._levelData.level.asteroidSpeed.Min, e._levelData.level.asteroidSpeed.Max), e._levelData.level.asteroidScore);
        if (spawnerPosition == SpawnerPosition.Right && count >= 4)
            FireAsteroid(new Vector3(-1f, 0f, Randomer.RandomFloat(0f, 1f)) * Randomer.RandomFloat(e._levelData.level.asteroidSpeed.Min, e._levelData.level.asteroidSpeed.Max), e._levelData.level.asteroidScore);
    }

    private void FireAsteroid(Vector3 target, int asteroidScore)
    {
        GameObject asteroid = Instantiate(asteroidPrefab, transform.position, Quaternion.identity, AsteroidsContainer.transform);
        StartCoroutine(UpdateAsteroid(asteroid, target, asteroidScore));
    }

    IEnumerator UpdateAsteroid(GameObject asteroid, Vector3 target, int asteroidScore)
    {
        yield return new WaitForSeconds(0f);

        asteroid.GetComponent<Asteroid>().SetScore(asteroidScore);
        PushAsteroid(asteroid, target);
    }

    private void PushAsteroid(GameObject asteroid, Vector3 target)
    {
        Rigidbody asteroid_rb = asteroid.GetComponent<Rigidbody>();
        asteroid_rb.AddForce(target);
    }

}

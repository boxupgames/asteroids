﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;
using UnityEngine.UI;

public class UIGameOver : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }

    void Start()
    {
        Events.OnGameStart += OnGameStart_Action;
        Events.OnPlayerLost += OnPlayerLost_Action;
        Events.OnGameEnd += OnGameEnd_Action;

        gameObject.SetActive(false);
    }

    private void OnPlayerLost_Action(object sender, EventArgs e)
    {
        GetComponent<Text>().text = "Game over! You are dead!";
        gameObject.SetActive(true);
    }

    private void OnGameEnd_Action(object sender, EventArgs e)
    {
        GetComponent<Text>().text = "You saved the planet!";
        gameObject.SetActive(true);
    }

    private void OnGameStart_Action(object sender, EventArgs e)
    {
        gameObject.SetActive(false);
    }

}

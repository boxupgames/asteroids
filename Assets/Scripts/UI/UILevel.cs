﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;
using UnityEngine.UI;

public class UILevel : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }

    void Start()
    {
        Events.OnUpdateScoresUI += OnUpdateScoresUI_Action;
    }

    private void OnUpdateScoresUI_Action(object sender, GameEvents.UpdateUIEventArgs e)
    {
        GetComponent<Text>().text = e._level.ToString();
    }

}

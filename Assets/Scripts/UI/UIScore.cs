﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;
using UnityEngine.UI;

public class UIScore : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }

    void Start()
    {
        Events.OnUpdateScoresUI += OnUpdateScoresUI_Action;
    }

    private void OnUpdateScoresUI_Action(object sender, GameEvents.UpdateUIEventArgs e)
    {
        string scores = "";
        for (int i = 0; i <= (6 - e._score.ToString().Length); i++)
        {
            scores = scores + "0";
        }

        GetComponent<Text>().text = scores + e._score.ToString();
    }


}

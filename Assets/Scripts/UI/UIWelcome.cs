﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;

public class UIWelcome : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }

    void Start()
    {
        Events.OnGameStart += OnGameStart_Action;
        Events.OnPlayerLost += OnPlayerLost_Action;
        Events.OnGameEnd += OnPlayerLost_Action;
    }

    private void OnGameStart_Action(object sender, EventArgs e)
    {
        gameObject.SetActive(false);
    }

    private void OnPlayerLost_Action(object sender, EventArgs e)
    {
        gameObject.SetActive(true);
    }

}

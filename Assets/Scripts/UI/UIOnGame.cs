﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;
using System;

public class UIOnGame : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }

    void Start()
    {
        Events.OnGameStart += OnGameStart_Action;

        gameObject.SetActive(false);
    }


    private void OnGameStart_Action(object sender, EventArgs e)
    {
        gameObject.SetActive(true);
    }


}

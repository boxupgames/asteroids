﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CryoDI;

public class Bullet : CryoBehaviour
{

    [Dependency]
    private GameEvents Events { get; set; }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 9)
        {
            other.gameObject.GetComponent<IEnemy>().Hit();
        }

        if (other.gameObject.name != "Spaceship" && other.gameObject.name != "WallReset")
            Destroy(gameObject);

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceFit : MonoBehaviour
{

    public Camera camera;
    public GameObject wallFront;
    public GameObject wallBack;
    public GameObject wallLeft;
    public GameObject wallRight;
    public GameObject wallReset;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        UpdateBorders();
    }


    private void UpdateBorders()
    {

        float ratio = (float)Screen.width / (float)Screen.height;

        float horizontalHalf = camera.ScreenToWorldPoint(new Vector3(Screen.width, 0f, 0f)).x;
        float horizontalFull = horizontalHalf * 2;
        float verticalHalf = horizontalHalf / ratio;
        float verticalFull = verticalHalf * 2;


        wallLeft.transform.position = new Vector3(-(horizontalHalf + .5f), wallLeft.transform.position.y, wallLeft.transform.position.z);
        wallLeft.transform.localScale = new Vector3(verticalFull, 1f, 1f);
        wallRight.transform.position = new Vector3(horizontalHalf + .5f, wallRight.transform.position.y, wallRight.transform.position.z);
        wallRight.transform.localScale = new Vector3(verticalFull, 1f, 1f);

        wallFront.transform.position = new Vector3(wallFront.transform.position.x, wallFront.transform.position.y, (verticalHalf + .5f));
        wallFront.transform.localScale = new Vector3(horizontalFull, 1f, 1f);
        wallBack.transform.position = new Vector3(wallBack.transform.position.x, wallBack.transform.position.y, -(verticalHalf + .5f));
        wallBack.transform.localScale = new Vector3(horizontalFull, 1f, 1f);

        wallReset.transform.localScale = new Vector3(verticalFull - .3f, 1f, horizontalFull - .3f);
    }
}
